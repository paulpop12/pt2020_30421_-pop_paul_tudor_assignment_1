package org.example;

import Utils.OperationsUtils;
import Utils.StringUtils;
import model.Polynomial;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * The AppTest class performs tests for each function of the calculator and checks if the results is the expected one.
 * There are 6 tests for each operation:
 *       - additionOp: p3 = p1 + p2
 *       - substractionOp: p3 = p1 - p2
 *       - derivOp: p3 = p1'
 *       - multiplicationOp: p3 = p1 * p2
 *       - intOp: p3 = S(p1)
 *       - divisionOp: p3 && rest  = p1 / p2
 */
public class AppTest
{
    Polynomial p1, p2, p3;
    String stringPolynomial1 = "4x^3-13x^2+2x-7";
    String stringPolynomial2 = "x^2+3x-2";
    ArrayList<Polynomial> polynomials = new ArrayList<>();

    @Before
    public void setUp() {
        p1 = StringUtils.getPoliFromString(stringPolynomial1);
        p2 = StringUtils.getPoliFromString(stringPolynomial2);
    }


    @Test
    public void additionOp() {
        p3 = OperationsUtils.additionOp(p1, p2);
        assertEquals("+4.0x^3-12.0x^2+5.0x-9.0", StringUtils.getStringFromPoli(p3));
    }


    @Test
    public void subtractionOp() {
        p3 = OperationsUtils.subtractionOp(p1, p2);
        assertEquals("+4.0x^3-14.0x^2-x-5.0", StringUtils.getStringFromPoli(p3));
    }

    @Test
    public void multiplicationOp() {
        p3 = OperationsUtils.multiplicationOp(p1, p2);
        assertEquals("+4.0x^5-x^4-45.0x^3+25.0x^2-25.0x+14.0", StringUtils.getStringFromPoli(p3));
    }

    @Test
    public void divisionOp() {

        polynomials = OperationsUtils.divisionOp(p1, p2);
        assertEquals("+4.0x-25.0", StringUtils.getStringFromPoli(polynomials.get(0)));
        assertEquals("+85.0x-57.0", StringUtils.getStringFromPoli(polynomials.get(1)));
    }

    @Test
    public void derivationOp() {
        p3 = OperationsUtils.derivOp(p1);
        assertEquals("+12.0x^2-26.0x+2.0", StringUtils.getStringFromPoli(p3));
    }


    @Test
    public void integrationOp() {
        p3 = OperationsUtils.intOp(p1);
        assertEquals("+x^4-4.3333335x^3+x^2-7.0x", StringUtils.getStringFromPoli(p3));
    }
}
