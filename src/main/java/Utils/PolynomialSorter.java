package Utils;

import model.Monomial;

import java.util.Comparator;


/** The PolynomialSorter class is used in order to sort the polynomial if it is given in the wrong order. It is sorted
* after the degree in order to display if from the biggest degree to the smallest. The subtraction
* mon2.getDegree() - mon1.getDegree() says which one is first.  If is bigger or equal to 0 then mon2 is first
* otherwise mon1 is first
 */

public class PolynomialSorter implements Comparator<Monomial> {
    @Override
    public int compare(Monomial mon1, Monomial mon2) {
        return mon2.getDegree() - mon1.getDegree();
    }
}
