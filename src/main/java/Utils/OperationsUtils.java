package Utils;

import model.Monomial;
import model.Polynomial;

import java.util.ArrayList;

/** The OperationsUtils holds all the operations which will be performed by the calculator + the removeZeros. So,
* this class contains the following functions:
*       - removeZeros
*       - additionOp: p3 = p1 + p2
*       - substractionOp: p3 = p1 - p2
*       - derivOp: p3 = p1'
*       - multiplicationOp: p3 = p1 * p2
*       - intOp: p3 = S(p1)
*       - divisionOp: p3 & rest  = p1 / p2
 */


public class OperationsUtils {

    /** The removeZeros function remove from an ArrayList all the Monomials
     * which are Monomials which have the coefficient equal to 0.
     */
    public static void removeZeros(ArrayList<Monomial> list) {
        list.removeIf(mon -> mon.getMultiplier().floatValue() == 0.0);
    }

    /** The additionOp function has as parameters 2 polynomials and returns a polynomial which represents the sum of the
     * polynomials received as parameters. It uses the merge algorithm.
     * It starts with 2 indexes(i and j) from 0 and keeps adding the monomials to the final polynomial.
     * If there is find a monomial with the same degree it's coefficient are summed up. and added to the final polynomial.
     *  If one of the indexes arrived at the end of the polynomial the all
     * the others elements from the other polynomial are added to the final element.
     */
    public static Polynomial additionOp(Polynomial pol1, Polynomial pol2) {
        int i = 0, j = 0;
        ArrayList<Monomial> finalPol = new ArrayList<>();
        while (i < pol1.getPoly().size() && j < pol2.getPoly().size()) {
            if (pol1.getPoly().get(i).getDegree() > pol2.getPoly().get(j).getDegree()) {
                finalPol.add(pol1.getPoly().get(i));
                i++;

            } else if (pol2.getPoly().get(j).getDegree() > pol1.getPoly().get(i).getDegree()) {
                finalPol.add(pol2.getPoly().get(j));
                j++;

            } else if (pol2.getPoly().get(j).getDegree() == pol1.getPoly().get(i).getDegree()) {
                double multiplier = pol1.getPoly().get(i).getMultiplier().doubleValue() + pol2.getPoly().get(j).getMultiplier().doubleValue();
                finalPol.add(new Monomial(pol1.getPoly().get(i).getDegree(), multiplier));
                i++;
                j++;
            }
        }
        while (i < pol1.getPoly().size()) {
            finalPol.add(pol1.getPoly().get(i));
            i++;
        }
        while (j < pol2.getPoly().size()) {
            finalPol.add(pol2.getPoly().get(j));
            j++;
        }
        removeZeros(finalPol);
        return new Polynomial(finalPol);
    }

    /** The subtractionOp function has as parameters 2 polynomials and returns a polynomial which
     * represents the difference of the polynomials received as parameters.
     * All the monomials from the second polynomial multiplied with -1 and then it is performed
     * the sum operation on them.
     */
    public static Polynomial subtractionOp(Polynomial pol1, Polynomial pol2) {

        for (Monomial mon : pol2.getPoly()) {
            mon.setMultiplier(mon.getMultiplier().doubleValue() * -1.0);
        }
        return additionOp(pol1, pol2);
    }

    /** The derivOp function has as parameters one polynomial and returns a polynomial which represents the derivative
     * form of the polynomial received as parameters.
     * For each monomial its degree is reduced by one and then the coefficient is multiplied with the old degree
     */

    public static Polynomial derivOp(Polynomial polynomial) {
        ArrayList<Monomial> monomials = new ArrayList<>();

        for (Monomial mon : polynomial.getPoly()) {
            monomials.add(new Monomial(mon.getDegree() - 1,
                    mon.getMultiplier().floatValue() * mon.getDegree()));

        }
        removeZeros(monomials);
        return new Polynomial((monomials));
    }
    /** The intOp function has as parameters one polynomial and returns a polynomial which represents the integrate
     * form of the polynomial received as parameters.
     *  For each monomial it's degree is increased by one and then the coefficient is divided with the new degree.
     */

    public static Polynomial intOp(Polynomial polynomial) {
        ArrayList<Monomial> monomials = new ArrayList<>();

        for (Monomial mon : polynomial.getPoly()) {
            monomials.add(new Monomial(mon.getDegree() + 1,
                    mon.getMultiplier().floatValue() / (mon.getDegree() + 1)));
        }
        removeZeros(monomials);
        return new Polynomial((monomials));
    }

    /** The multiplicationOp function has as parameters 2 polynomials and returns a polynomial which represents the product
     * of the polynomials received as parameters.
     * This operation is performed using 2 foreach loops multiply each monomial from pol2 to each monomial from pol1.
     * Each monomial obtained from the multiplication is added with sum to the final monomial. In this  way, the
     * duplicates won't appear.
     */
    public static Polynomial multiplicationOp(Polynomial pol1, Polynomial pol2) {
        ArrayList<Monomial> finalMonoms = new ArrayList<>();
        ArrayList<Monomial> additionMo = new ArrayList<>();
        Polynomial finalPolinom = new Polynomial(finalMonoms);
        Polynomial additionPolinom = new Polynomial();

        for (Monomial mon1 : pol1.getPoly()) {
            for (Monomial mon2 : pol2.getPoly()) {
                Monomial monomial = new Monomial((mon1.getDegree() + mon2.getDegree()),
                        mon1.getMultiplier().floatValue() * mon2.getMultiplier().floatValue());
                additionMo.clear();
                additionMo.add(monomial);
                additionPolinom.setPoly(additionMo);

                finalPolinom = additionOp(finalPolinom, additionPolinom);
            }
        }
        removeZeros(finalPolinom.getPoly());
        return finalPolinom;
    }


    /** The divisionOp function has as parameters 2 polynomials and returns 2 polynomials represents the result and the rest from the
     * division of the polynomials received as parameters. It uses the merge algorithm.
     * This operation is performed with a while loop in which 3 steps are made. The while loop is made until the grade
     * of the first polynomial is smaller then the degree of the second polynomial The 4 steps made are:
     *      - Divide  the highest degree monomial from p1 to the highest degree of p2
     *      - Multiply the result obtained at step 1 with p2
     *      - Subtract from the p1 the polynomial obtained at step 2
     *      - Replace the p1 with teh polynomial obtained at step 3
     * The rest will be in p1 and the final polynomial will be formed by the repetitive division of
     * the highest degree monomial from p1 to the highest degree of p2.
     *
     */
    public static ArrayList<Polynomial> divisionOp(Polynomial pol1, Polynomial pol2) {
        ArrayList<Monomial> resultedPolinom = new ArrayList<>();
        ArrayList<Monomial> intermitentPolin = new ArrayList<>();
        Polynomial pol1Cpy = pol1;

        while (pol1Cpy.getPoly().get(0).getDegree() >= pol2.getPoly().get(0).getDegree()) {
            Monomial monomial = new Monomial(pol1Cpy.getPoly().get(0).getDegree() - pol2.getPoly().get(0).getDegree(),
                    pol1Cpy.getPoly().get(0).getMultiplier().doubleValue() / pol2.getPoly().get(0).getMultiplier().doubleValue());

            intermitentPolin.clear();
            intermitentPolin.add(monomial);

            resultedPolinom.add(monomial);
            Polynomial p1 = new Polynomial(intermitentPolin);
            pol1Cpy = subtractionOp(pol1Cpy, multiplicationOp(p1, pol2));
        }

        ArrayList<Polynomial> result = new ArrayList<>();
        result.add(new Polynomial(resultedPolinom));
        result.add(pol1Cpy);
        return result;
    }

}
