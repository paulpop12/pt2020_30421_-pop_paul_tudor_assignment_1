package Utils;

import model.Monomial;
import model.Polynomial;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** The StringUtils class has the functions which works with strings. It has 3 functions:
* The CheckForm function which will check if the provided string from the text fields is correct and if it respects the
* needed form.
* The getPoliFromString function gets from a String the Polynomial needed, using Regex for separation. The regex
* helps to separate the text into 4 groups:
*       - The first group gets the sign of the coefficient of each monomial
*       - The second group gets the value of the coefficient
*       - The third and the fourth one only eliminates the x^ or x
*       - The fifth groups gets the grade of x. A special case is taken in consideration because at power 1 and 0
*        there is no ^ so it has to be treated differently. At power 1 it is tested if there is an x at third group
*
* The getStringFromPoli function gets from a Polynomial the String needed  to display it in the text field or in
* the label With a foreach every monomial is taken and split into sign variable and power variable.
*       - The sign variable stores the sign of the coefficient and after that it is added the value of the coefficient.
*       If it is equal to  -1 or 1 it is not added.
*       - The power variable stores the x^ (if needed, because at power 1 or 0  it is not needed and therefore it is
*       not added) and the value of the degree.
 */
public class StringUtils {

    public static boolean checkForm(String text) {
        if (text == null || text.equals(""))
            return false;
        else if (text.matches(".*[^0-9xX.+-^].*")) {
            return false;
        }else
            return true;
    }

    public static Polynomial getPoliFromString(String polyString) {
        ArrayList<Monomial> monomials = new ArrayList<>();

        if (polyString != null) {
            Pattern pattern = Pattern.compile("([-+]?)([0123456789.]*)([xX][\\^]?)?([0-9]?)");
            Matcher matcher = pattern.matcher(polyString);
            while (matcher.find()) {
                float multiplier = 1;
                int degree = 0;

                if (!matcher.group(2).equals("") && matcher.group(2) != null)
                    multiplier = Float.parseFloat(matcher.group(2));

                if (matcher.group(1).equals("-"))
                    multiplier *= -1;

                if (matcher.group(4) != null && !matcher.group(4).equals("")) {
                    degree = Integer.parseInt(matcher.group(4));

                } else if (matcher.group(3) != null)
                    if (matcher.group(3).equals("x"))
                        degree = 1;
                monomials.add(new Monomial(degree, multiplier));
            }
            monomials.remove(monomials.size() - 1);
        }
        return new Polynomial(monomials);
    }

    public static String getStringFromPoli(Polynomial polynomial) {
        StringBuilder text = new StringBuilder();
        for (Monomial mon : polynomial.getPoly()) {
            String sign = "";
            String power = "";

            if (mon.getMultiplier().doubleValue() >= 0)
                sign = "+";

            if (mon.getMultiplier().floatValue() != 1.0)
                sign = sign + mon.getMultiplier().floatValue();

            if (mon.getMultiplier().floatValue() == -1.0)
                sign = "-";

            if (mon.getDegree() >= 2)
                power = "x^" + mon.getDegree();
            else if (mon.getDegree() == 1)
                power = "x";


            text.append(sign).append(power);

        }
        return text.toString();
    }
}
