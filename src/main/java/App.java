import controller.Controller;


public class App 
{
    public static void main( String[] args ) {
        //The only thing needed for the application to open correctly is to create an instance of the Controller
        // which will create an instance of View and of the Model
       Controller controller = new Controller();

    }
}
