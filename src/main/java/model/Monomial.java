package model;

/** The Monomial class 2 fields:
 *  - The degree which stores the int value of the degree of the monomial
 *  - The multiplier which is a Number (in order to retrieve the needed form, either a float or a int or a double) and
 *  stores the coefficient of the monomial.
 *  It has getters and setters for each field, a default constructor and a constructor which gets the degree and
 *  the coefficient.
 */

public class Monomial {

    int degree;
    Number multiplier;

    public Monomial(int degree, Number multiplier) {
        this.degree = degree;
        this.multiplier = multiplier;
    }

    public Monomial() {
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    public Number getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(Number multiplier) {
        this.multiplier = multiplier;
    }
}
