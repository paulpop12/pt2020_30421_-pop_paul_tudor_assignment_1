package model;

import java.util.ArrayList;
/** The Polynomial class has an array of monomials with getters, setters, a default constructor and a constructor
 * which gets an array of Monomials.
 */
public class Polynomial {

    ArrayList<Monomial> poly;

    public Polynomial(ArrayList<Monomial> poly) {
        this.poly = poly;
    }

    public Polynomial() {
        this.poly = new ArrayList<>();
    }

    public ArrayList<Monomial> getPoly() {
        return poly;
    }

    public void setPoly(ArrayList<Monomial> poly) {
        this.poly = poly;
    }
}
