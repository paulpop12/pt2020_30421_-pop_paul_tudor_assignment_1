package controller;

import Utils.OperationsUtils;
import Utils.PolynomialSorter;
import Utils.StringUtils;
import model.Polynomial;
import view.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/** The Controller class stores the main control of the program. When an instance of the Controller is created
 * the controller creates an instance of the GUI and initialise the polynomials. The Controller class has an inner class
 * which is a ButtonClickListener.
 */

public class Controller {

    private static GUI view;
    private static Polynomial pol1, pol2, polResult;

    public Controller() {
        view = new GUI();
        pol1 = new Polynomial();
        pol2 = new Polynomial();
        polResult = new Polynomial();
    }

    /** The ButtonClickListener class implements ActionListener and has the purpose the get when a button is pressed.
     * It is implemented in the Controller and the buttons are in the view and communicates through listeners.
     * The command has the information of which button was pressed and the first thing that is checked is if the c
     * cancel button was pressed because is the only one with a special case.
     * At cancel the polynomials and the text views are restarted to the initial value.
     * If other button is pressed then form of the text is checked and then the polynomials are modified into
     * polynomials from string using the methods and then sorted.
     * At the end it is checked which operation is going to be performed
     */
    public static class ButtonClickListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            String command = actionEvent.getActionCommand();

            if (command.equals("cancel")) {
                pol1 = new Polynomial();
                pol2 = new Polynomial();
                polResult = new Polynomial();
                view.setRestText("Rest - only for division");

                view.setPol1Text("");
                view.setPol2Text("");
                view.setPolResult("");
            } else if (updateTexts(command)) {
                pol1.getPoly().sort(new PolynomialSorter());
                pol2.getPoly().sort(new PolynomialSorter());
                performSwitch(command);
            }
        }

        private void performSwitch(String command) {
            switch (command) {
                case "add":
                    polResult = OperationsUtils.additionOp(pol1, pol2);
                    break;
                case "subs":
                    polResult = OperationsUtils.subtractionOp(pol1, pol2);
                    break;
                case "multi":
                    polResult = OperationsUtils.multiplicationOp(pol1, pol2);
                    break;
                case "div":
                    ArrayList<Polynomial> polynomials = OperationsUtils.divisionOp(pol1, pol2);
                    polResult = polynomials.get(0);
                    view.setRestText(StringUtils.getStringFromPoli(polynomials.get(1)));
                    break;
                case "deriv":
                    polResult = OperationsUtils.derivOp(pol1);
                    break;
                case "int":
                    polResult = OperationsUtils.intOp(pol1);
                    break;
                default:
                    view.setErrorText("Wrong operation");

            }
            view.setPolResult(StringUtils.getStringFromPoli(polResult));

        }

        public static boolean updateTexts(String command) {
            if ((StringUtils.checkForm(view.getPol1()) && StringUtils.checkForm(view.getPol2())) ||
                    (StringUtils.checkForm(view.getPol1()) && (command.matches("deriv") || command.matches("int")))) {

                view.setErrorText("Correct form");
                view.setRestText("Rest - only for division");
                if (!polResult.getPoly().isEmpty()) {
                    pol1 = pol2;
                    pol2 = polResult;
                    polResult = new Polynomial();

                    view.setPol1Text(StringUtils.getStringFromPoli(pol1));
                    view.setPol2Text(StringUtils.getStringFromPoli(pol2));
                    view.setPolResult(StringUtils.getStringFromPoli(polResult));
                } else {
                    pol1 = StringUtils.getPoliFromString(view.getPol1());
                    pol2 = StringUtils.getPoliFromString(view.getPol2());
                }
                return true;

            } else {
                view.setErrorText("Error form");
                return false;
            }
        }

    }
}

