package view;

import controller.Controller;

import javax.swing.*;
import java.awt.*;

public class GUI {

    private JTextField poliOne;
    private JTextField poliTwo;
    private JLabel poliResult;
    private JLabel errorLabel;
    private JLabel restLabel;

    public GUI() {

        //The main frame is formed with smaller panels
        JFrame mainFrame = new JFrame("Polynomial calculator");
        mainFrame.setSize(700, 400);

        JPanel buttonPanel = new JPanel();
        JPanel inputPanel = new JPanel();
        JPanel finalPanel = new JPanel();
        JPanel labelPanel = new JPanel();
        JPanel addPanel = new JPanel();

        JLabel poliOneLabel = new JLabel("Polynom one:");
        JLabel poliTwoLabel = new JLabel("Polynom two:");
        JLabel spaceLabel =  new JLabel("  ");
        JLabel poliResLabel = new JLabel("Resulted polynom:");
        errorLabel = new JLabel("No errors");


        /** The first panel is the Label Panel which is used for displaying 4 texts
         * which indicates information about a text field(poliOneLabel, poliTwoLabel, poliResultLabel)
         * or if the text field information are introduced correctly(errorLabel)
         */
        labelPanel.add(poliOneLabel);
        labelPanel.add(poliTwoLabel);
        labelPanel.add(poliResLabel);
        labelPanel.add(spaceLabel);
        labelPanel.add(errorLabel);
        labelPanel.setLayout(new GridLayout(5, 1));

        /**The second panel is the one that holds all the buttons which are 6 operations and one for
         * cancel the commands and clear the text fields.
         */
        JButton addButton = new JButton("+");
        JButton multiplicationButton = new JButton("*");
        JButton subtractButton = new JButton("-");
        JButton divButton = new JButton("/");
        JButton derivButton = new JButton("'");
        JButton intButton = new JButton("S");
        JButton cancelButton = new JButton("C");

        //added action command for the buttons in order to identify which button is pressed
        cancelButton.setActionCommand("cancel");
        addButton.setActionCommand("add");
        subtractButton.setActionCommand("subs");
        multiplicationButton.setActionCommand("multi");
        divButton.setActionCommand("div");
        derivButton.setActionCommand("deriv");
        intButton.setActionCommand("int");

        //added action command for the buttons in order to identify which button is pressed
        addButton.addActionListener(new Controller.ButtonClickListener());
        subtractButton.addActionListener(new Controller.ButtonClickListener());
        multiplicationButton.addActionListener(new Controller.ButtonClickListener());
        divButton.addActionListener(new Controller.ButtonClickListener());
        derivButton.addActionListener(new Controller.ButtonClickListener());
        intButton.addActionListener(new Controller.ButtonClickListener());
        cancelButton.addActionListener(new Controller.ButtonClickListener());

        //ToolTip displays if the user hovers over the button to get details about that button
        addButton.setToolTipText("Addition");
        subtractButton.setToolTipText("Subtraction");
        multiplicationButton.setToolTipText("Multiplication");
        divButton.setToolTipText("Division");
        intButton.setToolTipText("Integration");
        derivButton.setToolTipText("Derivative");
        cancelButton.setToolTipText("Derivative");


        buttonPanel.add(addButton);
        buttonPanel.add(subtractButton);
        buttonPanel.add(multiplicationButton);
        buttonPanel.add(divButton);
        buttonPanel.add(derivButton);
        buttonPanel.add(intButton);
        buttonPanel.add(cancelButton);
        buttonPanel.setLayout((new GridLayout(1, 7)));


        //The third panel contains the text fields and the jlabels for results
        poliOne = new JTextField(40);
        poliTwo = new JTextField(40);
        poliResult = new JLabel("Choose an operation");
        restLabel = new JLabel("Rest - only for division");


        inputPanel.add(poliOne);
        inputPanel.add(poliTwo);
        inputPanel.add(poliResult);
        inputPanel.add(restLabel);
        inputPanel.setLayout(new GridLayout(4, 1));

        finalPanel.add(labelPanel);
        finalPanel.add(inputPanel);
        finalPanel.add(buttonPanel);
        finalPanel.setLayout(new FlowLayout());

        addPanel.add(finalPanel);
        addPanel.setLayout(new GridLayout(2, 1));


        mainFrame.setLayout(new GridLayout(1, 1));
        mainFrame.setContentPane(addPanel);
        mainFrame.setVisible(true);
    }

    /** In order to respect the principles of OOP I added getters and setters for the text
      * fields and for the results label
     */
    public String getPol1(){
        return poliOne.getText();
    }

    public String getPol2(){
        return poliTwo.getText();
    }

    public void setPol1Text(String text){
        this.poliOne.setText(text);
    }

    public void setPol2Text(String text){
        this.poliTwo.setText(text);
    }


    public String getPolResult() {
        return poliResult.getText();
    }

    public void setPolResult(String poliResult) {
        this.poliResult.setText(poliResult);
    }

    public void setErrorText(String text){
        errorLabel.setText(text);
    }

    public void setRestText(String rest) {
        this.restLabel.setText(rest);
    }

}
